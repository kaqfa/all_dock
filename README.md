### Prosedur Standar Pelaksanaan

- Pertama buat network local dulu dengan nama localnet `docker network create localnet`
- Selanjutnya, install database yang diperlukan saja, tidak harus semuanya
- Baru install aplikasi satu persatu sesuai kebutuhan

### Membuat proyek baru

- Semua database sebaiknya nama direktorinya ditambahkan db_ di depannya
- Semua proyek odoo ditambahkan odoo_ di depannya
- Semua proyek django ditambahkan dj_ di depannya
- Semua proyek aplikasi diberikan README.md untuk keterangan repository kode sumbernya
- Kode sumber utama disimpan dalam direktori `src`, kode sumber tambahan disimpan dalam direktori `more-src`
- Kedua direktori tersebut di-ignore pada git ini, sehingga perubahan pada kode sumber wajib di push dari dalam src
- Jika perlu dockerfile, silahkan dibuat kalau bisa